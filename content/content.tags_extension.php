<?php

	require_once(TOOLKIT . '/class.administrationpage.php');
	require_once(TOOLKIT . '/class.eventmanager.php');
	require_once CONTENT . '/class.sortable.php';
	// require_once(TOOLKIT . '/class.jsonpage.php');

	Class contentExtensionTags_ManagerTags_Extension extends AdministrationPage {

		public function __viewIndex() {
			$this->setPageType('table');
			$this->setTitle(__('%1$s &ndash; %2$s', array(__('Symphony'), __('Tags Manager'))));
			
			$this->appendSubheading(__('Tags Manager')
			);

			$columns[] = array(
				'label' => __('Name'),
				// 'sortable' => true,
				'handle' => 'value',
				'attrs' => array(
					'id' => 'field-name'
				)
			);

			$columns[] = array(
				'label' => __('handle'),
				// 'sortable' => true,
				'handle' => 'handle',
				'attrs' => array(
					'id' => 'field-handle'
				)
			);
	
			$columns[] = array(
				'label' => __('count'),
				// 'sortable' => true,
				'handle' => 'count',
				'attrs' => array(
					'id' => 'field-count'
				)
			);

			$columns[] = array(
				'label' => __('link'),
				// 'sortable' => true,
				'handle' => 'link',
				'attrs' => array(
					'id' => 'field-link'
				)
			);
		

			$aTableHead = Sortable::buildTableHeaders($columns, $_GET['sort'], $_GET['order'], ($filter_querystring) ? "&amp;" . $filter_querystring : '');

			$aTableBody = array();

			$dateFormat = Symphony::Configuration()->get('date_format','region') . ' ' .  Symphony::Configuration()->get('time_format','region');

			$favourites = TagsManager::fetch();

			// var_dump($favourites);die;  

			if(!is_array($favourites) || empty($favourites)){
				$aTableBody = array(Widget::TableRow(
					array(Widget::TableData(__('None found.'), 'inactive', NULL, count($aTableHead)))
				));
			} else {

				$i = 0;

				foreach($favourites as $favourite){

					if($favourite["handle"] != NULL && $favourite["value"] != NULL){

						$columns = array();
						if (Symphony::Author()->isDeveloper()){
							$columns[] = Widget::TableData(Widget::Anchor(
								$favourite['value'], Administration::instance()->getCurrentPageURL().'edit/' . $favourite['handle'] . '/', null, 'content'
							));
						} else {
							$columns[] = Widget::TableData($favourite['value']);
						}
						$columns[] = Widget::TableData($favourite['handle']);
						$columns[] = Widget::TableData($favourite['count']);
						$columns[] = Widget::TableData($favourite['link']);
									
						$aTableBody[] = Widget::TableRow($columns,null, "id-" . $favourite['id'] );

						$i++;

					}
				
				}
			}

			$table = Widget::Table(
				Widget::TableHead($aTableHead),
				NULL,
				Widget::TableBody($aTableBody),
				'selectable'
			);

			$scriptContent =	"jQuery(document).ready(function(){
									jQuery('table').symphonySelectable();
								});";
			$script = new XMLElement('script',$scriptContent);
			$this->addElementToHead($script);

			$this->Form->appendChild($table);

			$tableActions = new XMLElement('div');
			$tableActions->setAttribute('class', 'actions');

			$options = array(
				0 => array(null, false, __('With Selected...')),
				1 => array('delete', false, __('Delete'), 'confirm'),
			);

			$tableActions->appendChild(Widget::Apply($options));
			$this->Form->appendChild($tableActions);
		}


		public function __viewNew() {
			$this->__viewEdit();
		}

		public function __viewEdit() {

			$isNew = true;
			$time = Widget::Time();

			$tag_handle= $this->_context[1];
			$tag_details = TagsManager::fetch($tag_handle);
			$tag_name = $tag_details["value"];
			
			$this->setPageType('form');

			$fields = array();
			$this->insertBreadcrumbs(array(Widget::Anchor(__('Tags'), extension_Tags_Manager::baseURL() . 'tags_extension/')));
		
			$this->setTitle(__('Symphony &ndash; Edit Tag'));
			$this->appendSubheading($tag_name);

			$fieldset = new XMLElement('fieldset');

			if (isset($tag_handle))
				$fieldset->appendChild(Widget::Input('handle', $tag_handle, 'hidden'));
			


			$primary = new XMLElement('div',null,array('class' => 'primary column'));
			$fieldset->appendChild($primary);

			$primary->appendChild('
						<div id="field-29" class="field field-taglist" style="width: 200px;">
							<label>Merge with Tag 							
								<input name="fields[tag-merge]" type="text" value="" style="width:200px">
							</label>
						</div>					
						');


			// Add the actions:
			$actions = new XMLElement('div');
			$actions->appendChild(	"<div>
									<p><strong>Delete: </strong> delete the tag and redirect the tag page to Forward.com</p>
									<p><strong>Merge Tags: </strong> replace the tag and redirect to the new tag page (All articles that had the old tag will be 
									re-tagged with the new tag)</p>
									</div>");
			$actions->setAttribute('class', 'actions');
			$actions->appendChild(Widget::Input('action[merge]', __('Merge Tags'), 'submit', array('accesskey' => 's')));
			$actions->appendChild(Widget::Input('action[delete]', __('Delete Tag '.$tag_name), 'submit', array('accesskey' => 's', 'style' => 'float:left')));

			$this->Form->appendChild($fieldset);
			$this->Form->setAttribute('class','two columns');						
		
			$fieldset->appendChild($actions);
		}


		public function __actionEdit() {

			if(array_key_exists('delete', $_POST['action'])) {
				return $this->__actionDelete($this->_context[1]);
			}


			if(array_key_exists('merge', $_POST['action'])) {
				return $this->__actionMerge($this->_context[1]);
			}

			
		}

		public function __actionDelete($tag_handle = null) {

			$result = Symphony::Database()->insert(
			    array(
			        'tag_from' => $tag_handle,
			        'tag_to' => 'homepage'
			    ),
			    'sym_tags_redirect'
			);

			//add some modal before call this	?
			$queryDelete = "DELETE FROM sym_entries_data_29 where handle = '{$tag_handle}'";
			$queryDeleteResult = Symphony::Database()->fetch($queryDelete);					

			//give feedback
			redirect(extension_Tags_Manager::baseURL() . 'tags_extension/');
			
		}

		public function __actionMerge($tag_from = null) {
			
			$tag_to = MySQL::cleanValue($_POST['fields']['tag-merge']);

			$result = Symphony::Database()->insert(
			    array(
			        'tag_from' => $tag_from,
			        'tag_to' => $tag_to
			    ),
			    'sym_tags_redirect'
			);
			
			$query_to = "SELECT handle, value FROM sym_entries_data_29 WHERE handle = '{$tag_to}' LIMIT 1;";	
			$queryToResult = Symphony::Database()->fetchRow(0,$query_to);

			$newName= $queryToResult["value"];
			$newHandle= $queryToResult["handle"];
			
			$queryUpdate = "UPDATE sym_entries_data_29 SET value = '{$newName}', handle= '{$newHandle}' WHERE handle = '{$tag_from}';";
			$queryToResult = Symphony::Database()->fetch($queryUpdate);	

		
			//give feedback
			redirect(extension_Tags_Manager::baseURL() . 'tags_extension/');	
		}

	
	}

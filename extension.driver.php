<?php

include_once(EXTENSIONS . '/tags_manager/lib/class.tags_manager.php');

class extension_Tags_Manager extends Extension {

	public function fetchNavigation()
	{
		$navigation = array(
			array(
				'location'  => __('System'),
				'name'      => __('Tags Manager'),
				'link'      => '/tags_extension/'
			)
		);

		// $favourites = FavouriteManager::fetch();

		// $group = array();

		// foreach ($favourites as $favourite) {

		// 	if ( empty($group) || $group['name'] != $favourite['location']){
		// 		if (!empty($group)) {
		// 			$navigation[] = $group;
		// 		}

		// 		$group = array(
		// 			'name' => 	$favourite['location'],
		// 			'children' => array(),
		// 			'type' => 'content',
		// 			'index' => 1
		// 		);
		// 	}

		// 	$link = $favourite['link'];
		// 	$relative = 'yes';

		// 	$navItem = array(
		// 			'location'  => $favourite['location'],
		// 			'name'      => $favourite['name'],
		// 			'link'      => $link,
		// 			'relative'  => false,
		// 		);


		// 	$group['children'][] = $navItem;
		// }

		// if (!empty($group))
		// 	$navigation[] = $group;

		return $navigation;
	}
	
	public function getSubscribedDelegates() {
		return array(
			array(
				'page' => '/backend/',
				'delegate' => 'InitialiseAdminPageHead',
				'callback' => 'initializeAdmin',
			),
		);
	}
	
	/**
	 * Some admin customisations
	 */
	public function initializeAdmin($context) {

		$page = Administration::instance()->Page;
		$assets_path = URL . '/extensions/tags_manager/assets';

		$callback = Symphony::Engine()->getPageCallback();

		// echo "<pre>";
		// print_r($page);
		// echo "</pre>";die;

		// var_dump($page->_context[0]);die;
				
		// Only load on /publish/static-pages/ [this should be a variable]
		// if ($page->_context['section_handle'] == 'pages' && $page->_context['page'] == 'index') {
		if (get_class($page) == 'contentExtensionTags_ManagerTags_Extension'){
			$page->addStylesheetToHead($assets_path . '/tagui.selector.publish.css');
			$page->addScriptToHead($assets_path . '/tagui.selector.publish.js');
		}
		
	}
		
	
	/**
	 * Installation
	 */
	public function install() {
		// Roles table:
		Symphony::Database()->query("
			CREATE TABLE IF NOT EXISTS `tbl_tags_redirect` (
				`id` INT(11) unsigned NOT NULL auto_increment,		
				`tag_from` VARCHAR(255) NOT NULL,
				`tag_to` VARCHAR(255) NOT NULL,
				PRIMARY KEY (`id`)
			);		
		");
	}

	public static function baseURL(){
		return SYMPHONY_URL . '/extension/tags_manager/';
	}

}
?>